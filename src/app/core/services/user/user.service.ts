import { Injectable } from '@angular/core';
import { CryptoService } from '../../services/crypto/crypto.service';
import { UserHttpService } from '../../http/user/user-http.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	constructor(
		private userHttpService: UserHttpService,
		private cryptoService: CryptoService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService,
		private router: Router
	) {}

	async login(username: string, password: string) {
		console.log('Login');
		try {
			const key = this.cryptoService.genRandomString(32);

			const [usernameHash, passwordHash] = await Promise.all([
				this.cryptoService.hash(username, '$2a$10$2RNNkME2r9HR8YJPvAJdA.'),
				this.cryptoService.hash(password, '$2a$10$tBAXtqLmT9pS6qkyn866u.'),
			]);

			const serverSidePassword = await this.cryptoService.hash(passwordHash, '$2a$10$UuZnbPU2UZbNsWLqnLVyC.');

			const masterEncryptionKeyEncrypted = await this.userHttpService.login(usernameHash, serverSidePassword);

			const masterEncryptionKey = await this.cryptoService.decryptAES(masterEncryptionKeyEncrypted, passwordHash, 'string');

			const [encryptedMetainfo, encryptedMasterEncryption, encryptedKey] = await Promise.all([
				this.cryptoService.encryptAES(`${navigator.platform}, ${navigator.appCodeName}`, masterEncryptionKey),
				this.cryptoService.encryptAES(masterEncryptionKey, key),
				this.cryptoService.hash(key, '$2a$10$9QUgTnFAB5tcCprTb2F6t.'),
			]);

			await this.userHttpService.createSession(
				encryptedKey,
				usernameHash,
				serverSidePassword,
				encryptedMasterEncryption,
				encryptedMetainfo
			);

			localStorage.setItem('sessionKey', key);

			//await this.refresh();
			//this.loginSubject.next();

			this.snackBar.open(this.translateService.instant('simple.loginSuccessful'), '', { duration: 3000 });
			this.router.navigate(['/core/home']);
		} catch (error) {
			console.log(error);
			//this.logService.error('Login failed.', 'error.login.failed');
		}
	}
}
