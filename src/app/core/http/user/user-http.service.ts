import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AESEncrypted } from 'src/app/core/models/aes.encrypted';

@Injectable({
	providedIn: 'root',
})
export class UserHttpService {
	private SERVICE = 'https://connect.kominal.com/user-service';

	constructor(private httpClient: HttpClient) {}

	login(username: string, password: string): Promise<AESEncrypted> {
		return this.httpClient
			.post<AESEncrypted>(`${this.SERVICE}/login`, { username, password })
			.toPromise();
	}

	createSession(key: string, username: string, password: string, masterEncryptionKey: AESEncrypted, device: AESEncrypted): Promise<void> {
		return this.httpClient
			.post<void>(`${this.SERVICE}/sessions`, {
				key,
				username,
				password,
				masterEncryptionKey,
				device,
			})
			.toPromise();
	}
}
