import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreComponent } from './modules/core/core.component';
import { AuthenticationComponent } from './modules/authentication/authentication.component';

const routes: Routes = [
	{ path: 'core', component: CoreComponent, loadChildren: () => import('./modules/core/core.module').then((m) => m.CoreModule) },
	{
		path: 'authentication',
		component: AuthenticationComponent,
		loadChildren: () => import('./modules/authentication/authentication.module').then((m) => m.AuthenticationModule),
	},
	{ path: '**', redirectTo: 'authentication' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
