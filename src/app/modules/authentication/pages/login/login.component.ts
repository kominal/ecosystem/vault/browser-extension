import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
	form: FormGroup;

	constructor(formBuilder: FormBuilder, private userService: UserService, private translateService: TranslateService) {
		this.form = formBuilder.group({
			username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
			password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
		});
	}

	async onSubmit() {
		const username = this.form.get('username');
		const password = this.form.get('password');

		if (!username) {
			throw new Error('Username is undefined');
		} else if (!password) {
			throw new Error('Password is undefined');
		}

		await this.userService.login(username.value, password.value);
	}

	getUsernameErrors(): string {
		const field = this.form.get('username');

		if (!field) {
			throw new Error('Field is undefined');
		}

		if (field.hasError('required')) {
			return this.translateService.instant('error.required');
		} else if (field.hasError('minlength')) {
			return this.translateService.instant('error.minLength', { minLength: field.getError('minlength').requiredLength });
		} else if (field.hasError('maxlength')) {
			return this.translateService.instant('error.maxLength', { maxLength: field.getError('maxlength').requiredLength });
		}
		return '';
	}

	getPasswordErrors(): string {
		const field = this.form.get('password');

		if (!field) {
			throw new Error('Field is undefined');
		}

		if (field.hasError('required')) {
			return this.translateService.instant('error.required');
		} else if (field.hasError('minlength')) {
			return this.translateService.instant('error.minLength', { minLength: field.getError('minlength').requiredLength });
		} else if (field.hasError('maxlength')) {
			return this.translateService.instant('error.maxLength', { maxLength: field.getError('maxlength').requiredLength });
		}
		return '';
	}
}
