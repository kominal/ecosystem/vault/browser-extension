import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';
import { LoginComponent } from './pages/login/login.component';
import { SharedModule } from 'src/app/core/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [AuthenticationComponent, LoginComponent],
	imports: [CommonModule, AuthenticationRoutingModule, TranslateModule, SharedModule, FormsModule, ReactiveFormsModule],
})
export class AuthenticationModule {}
