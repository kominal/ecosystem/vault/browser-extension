import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { HomeComponent } from './pages/home/home.component';
import { SharedModule } from 'src/app/core/shared.module';
import { GeneratorComponent } from './pages/generator/generator.component';
import { VaultComponent } from './pages/vault/vault.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
	declarations: [CoreComponent, HomeComponent, GeneratorComponent, VaultComponent],
	imports: [CommonModule, CoreRoutingModule, TranslateModule, SharedModule, FormsModule, ReactiveFormsModule],
})
export class CoreModule {}
