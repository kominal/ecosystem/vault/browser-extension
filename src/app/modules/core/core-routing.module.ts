import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { VaultComponent } from './pages/vault/vault.component';
import { GeneratorComponent } from './pages/generator/generator.component';

const routes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'vault', component: VaultComponent },
	{ path: 'generator', component: GeneratorComponent },
	{ path: '**', redirectTo: 'home' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class CoreRoutingModule {}
